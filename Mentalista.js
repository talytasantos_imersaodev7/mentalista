var numeroSecreto = parseInt(Math.random() * 11);
var tentativas = 3;

alert("Você tem " + tentativas + " tentativas");

    while (tentativas > 0){
        var chute = prompt('Digite um número entre 0 e 10');
      
        if (chute == numeroSecreto) {
          alert("Acertou!");
          break;   
        } else if (chute > numeroSecreto){
          alert("Errou... O número secreto é menor que o chute");
        } else if (chute < numeroSecreto){
          alert("Errou... O número secreto é maior que o chute");
        }
    
        tentativas--;

        if (tentativas === 0) {
            alert("Suas tentativas acabaram. O número secreto era: " + numeroSecreto);
        } else {
            alert("Você tem mais " + tentativas + " tentativa(s) restante(s)");
        }

    }       